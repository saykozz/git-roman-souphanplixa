#/bin/bash


# Copier le fichier de configuration sur le serveur

cp ./config__files/ssh/sshd_config /etc/ssh/sshd_config

cp ./config__files/ssh/Banner /etc/Banner


# Redemarrer le service sshd 

systemctl restart sshd 

#Ajouter un nouveau users Linux et l'ajouter au groupe sudo 
echo "Quel est le nom du user que vous voulez créer ?"
read USER
sudo useradd $USER && usermod -aG sudo $USER

#Intégration d'un clé ssh

echo "SSH KEYS CONFIG"
echo "Veuillez renseigner un mail"
read MAIL
cd /.ssh/
#ssh-keygen -t rsa -b 4096 -C "$MAIL"
ssh-keygen -t rsa -b 4096 -C "$MAIL" -N '' -f ${USER}_rsa &&
echo "#tmp_rsa" >> authorized_keys &&
cat ${USER}_rsa.pub >> authorized_keys &&
cat authorized_keys &&
service ssh restart &&
echo "OK"
